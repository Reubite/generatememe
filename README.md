This meme-generating program is structured in such a way that all the activities extend the main activity. This design structure was actually learned and suggested from a 
fellow course mate. It eases access to the recycleView and all the meme objects.

The main java package is divided into 2 parts for easy smooth programming experience and ease to have access 
to the meme object itself (Similar to a Quiz app discussed in one of our classes): Activities and Models.

An array list of memes of MemeCreatorModel type was used to store the initial (existing) memes and then 
was used to add every newly generated meme into it (the memes arrayList).
A meme object took in a URL, and two different text parameters, then passed them to the image downloaded. 

Picasso (library) was used to load the url image into the view holder. I used a recycleView as discussed in the course from which the ViewHolder extends. 

I utilized Stackoverflow, and got ideas from a fellow coursemate about how to edit and remove the texts on existing memes. Therefore, a long hold on each existing meme will display a dialog to edit or remove (delete) the selected meme. Butter Knife was utilized, easing the idea of binding views, etc.  
 
The app simply gets a url image (from the URL text input), upper text, and lower text; then loads these data to generate a meme.