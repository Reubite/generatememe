package com.reubenajayi.memegenerator.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.Toast;

import com.reubenajayi.memegenerator.Models.MemeCreatorModel;
import com.reubenajayi.memegenerator.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by r09003799 on 11/14/2016.
 */

public class EditMeme extends MainActivity {

    EditText murl, topText, bottomText;
    public int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_meme);

        Toolbar toolbar = (Toolbar) findViewById(R.id.memify_toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        murl = (EditText) findViewById(R.id.url);
        topText = (EditText) findViewById(R.id.top_text);
        bottomText = (EditText) findViewById(R.id.bottom_text);

        if (extras != null) {
            topText.setText(extras.getString("TOP_TEXT"));
            bottomText.setText(extras.getString("BOTTOM_TEXT"));
            murl.setText(extras.getString("URL_TEXT"));
            this.position = extras.getInt("ITEM_POSITION");
        }
        ButterKnife.bind(this);
    }

    @OnClick(R.id.edit_meme)
    public void edit() {
        if (!isFieldEmpty(murl) && !isFieldEmpty(topText) && !isFieldEmpty(bottomText)) {
            // Modify the meme
            MemeCreatorModel editMeme = memes.get(position);
            editMeme.textTop = topText.getText().toString();
            editMeme.textBottom = bottomText.getText().toString();
            editMeme.url = murl.getText().toString();
            saveCurrentMemes();

            Intent intent = new Intent(EditMeme.this, MainActivity.class);
            startActivity(intent);
        }
    }

    public boolean isFieldEmpty(EditText field) {
        boolean isEmpty = field.getText().toString().trim().equals("");
        if (isEmpty) {
            Toast.makeText(this, R.string.empty_field, Toast.LENGTH_SHORT).show();
            field.requestFocus();
            return true;
        }
        return false;
    }
}
