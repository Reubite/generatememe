package com.reubenajayi.memegenerator.Activities;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;
import com.reubenajayi.memegenerator.R;
import com.squareup.picasso.Picasso;

public class ViewMeme extends MainActivity {
    public TextView top;
    public TextView bottom;
    public ImageView url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_meme);

        Toolbar toolbar = (Toolbar) findViewById(R.id.view_memes_toolbar);
        setSupportActionBar(toolbar);

        top = (TextView) findViewById(R.id.top);
        bottom = (TextView) findViewById(R.id.bottom);
        url = (ImageView) findViewById(R.id.image);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        // Get the data from Meme Generator activity
        if (extras != null) {
            top.setText(extras.getString("TOP"));
            bottom.setText(extras.getString("BOTTOM"));
            String url_string = extras.getString("URL");
            Picasso.with(this).load(url_string).fit().into(url);
        }

    }
}
