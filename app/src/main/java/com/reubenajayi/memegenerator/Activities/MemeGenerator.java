package com.reubenajayi.memegenerator.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.reubenajayi.memegenerator.Models.MemeCreatorModel;
import com.reubenajayi.memegenerator.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MemeGenerator extends MainActivity {
    // Bind buttons for OnClick event
    @BindView(R.id.start_memify) View memify;

    // UI Elements
    EditText url, topText, bottomText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meme_generator);

        Toolbar toolbar = (Toolbar) findViewById(R.id.memify_toolbar);
        setSupportActionBar(toolbar);

        url = (EditText) findViewById(R.id.url);
        topText = (EditText) findViewById(R.id.top_text);
        bottomText = (EditText) findViewById(R.id.bottom_text);

        ButterKnife.bind(MemeGenerator.this);
    }

    @OnClick(R.id.start_memify)
    public void memify() {
        // Check if the inputs field are empty, if any of the fields is empty, it will set focus on that field
        if (!isFieldEmpty(url) && !isFieldEmpty(topText) && !isFieldEmpty(bottomText)) {
            // Create the memes save it to SharedPreferences
            memes.add(new MemeCreatorModel(url.getText().toString(), topText.getText().toString(), bottomText.getText().toString()));
            saveCurrentMemes();

            // Bring the users back to the Memes List
            Intent intent = new Intent(MemeGenerator.this, MainActivity.class);
            startActivity(intent);
        }
    }

    // Return true for empty, and false if not empty
    public boolean isFieldEmpty(EditText field) {
        boolean isEmpty = field.getText().toString().trim().equals("");
        if (isEmpty) {
            Toast.makeText(this, R.string.empty_field, Toast.LENGTH_SHORT).show();
            field.requestFocus();
            return true;
        }
        return false;
    }
}
