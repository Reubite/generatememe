package com.reubenajayi.memegenerator.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reubenajayi.memegenerator.Models.MemeCreatorModel;
import com.reubenajayi.memegenerator.R;
import com.squareup.picasso.Picasso;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnCreateContextMenuListener, View.OnLongClickListener {
    private RecyclerView recyclerView;
    public static List<MemeCreatorModel> memes;
    public static int itemPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_drawer);

        // Drawing Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Drawing floating button for adding new memes
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MemeGenerator.class);
                startActivity(intent);
            }
        });

        // Recycle view to inflate the the list of memes
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new Adapter(this));

        // Get the saved memes from SharedPreference or initialize new meme list
        if (savedInstanceState != null) {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            Gson gson = new Gson();
            String json = sharedPref.getString("memes", null);
            Type type = new TypeToken<ArrayList<MemeCreatorModel>>() {}.getType();
            memes = gson.fromJson(json, type);
        }
        else {
            String json = PreferenceManager.getDefaultSharedPreferences(this).getString("memes", null);
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<MemeCreatorModel>>() {}.getType();
            memes = (ArrayList) (json != null ? gson.fromJson(json, type) : new ArrayList<>());
        }

        // Initialize dummy memes
        if (memes.size() == 0) {
            memes.add(new MemeCreatorModel("https://imgflip.com/s/meme/Creepy-Condescending-Wonka.jpg",
                    "I changed all my passwords to 'incorrect.",
                    "I forget, it shows 'Your password is incorrect.'"));
            memes.add(new MemeCreatorModel("https://imgflip.com/s/meme/Black-Girl-Wat.jpg",
                    "...",
                    "I farted, So??"));
            memes.add(new MemeCreatorModel("http://static2.businessinsider.com/image/56e3189152bcd0320c8b5cf7-480/sammy-griner-success-kid-meme.jpg",
                    "I am so awesome", "Can't you tell?!"));
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);

        itemPosition = recyclerView.indexOfChild(v);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                editMeme(itemPosition);
                return true;
            case R.id.delete:
                deleteMeme(itemPosition);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void editMeme(int itemPosition) {

        MemeCreatorModel meme = memes.get(itemPosition);

        Intent intent = new Intent(MainActivity.this, EditMeme.class);
        intent.putExtra("URL_TEXT", meme.url);
        intent.putExtra("TOP_TEXT", meme.textTop);
        intent.putExtra("BOTTOM_TEXT", meme.textBottom);
        intent.putExtra("ITEM_POSITION", itemPosition);
        startActivity(intent);
    }


    private void deleteMeme(int itemPosition) {
        memes.remove(itemPosition);
        saveCurrentMemes();

        recreate();
    }

    public void saveCurrentMemes() {
        PreferenceManager.getDefaultSharedPreferences(this)
                .edit()
                .putString("memes", new Gson().toJson(memes))
                .apply();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("memes", new Gson().toJson(memes));
    }

    @Override
    public boolean onLongClick(View view) {
        return false;
    }

    public class Adapter extends RecyclerView.Adapter<ViewHolder> {
        private final Activity context;

        public Adapter(Activity context)  {
            this.context = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = context.getLayoutInflater().inflate(R.layout.activity_main_activity, parent, false);
            view.setOnClickListener(new MyOnClickListener());
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            MemeCreatorModel meme = memes.get(position);
            holder.top.setText(meme.textTop);
            holder.bottom.setText(meme.textBottom);
            Picasso.with(context).load(meme.url).into(holder.avatar);
        }

        @Override
        public int getItemCount() {
            return memes.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public final ImageView avatar;
        public final TextView top;
        public final TextView bottom;

        public ViewHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.iconImage);
            top = (TextView) itemView.findViewById(R.id.top);
            bottom = (TextView) itemView.findViewById(R.id.bottom);
            itemView.setOnCreateContextMenuListener(MainActivity.this);
            itemView.setOnLongClickListener(MainActivity.this);
        }
    }

    class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int itemPosition = recyclerView.indexOfChild(v);
            MemeCreatorModel meme = memes.get(itemPosition);
            Intent intent = new Intent(MainActivity.this, ViewMeme.class);
            intent.putExtra("URL", meme.url);
            intent.putExtra("TOP", meme.textTop);
            intent.putExtra("BOTTOM", meme.textBottom);
            startActivity(intent);
        }
    }
}
